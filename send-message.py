# pip install python-jenkins
# from jenkins import Jenkins
# import requests
# import os

# JENKINS_URL = "http://localhost:8080"
# USERNAME = "monster"
# PASSWORD = "123456"
# JOB_NAME = "job/selenium"

# # 飞书机器人 Webhook 地址
# FEISHU_WEBHOOK = "https://open.feishu.cn/open-apis/bot/v2/hook/4ccaa108-b719-4fa8-b7a9-98b434e3e938"

# def push_message():
#     try:
#         # 链接 jenkins
#         server = Jenkins(url=JENKINS_URL, username=USERNAME, password=PASSWORD, timeout=10)
#         job_url = server.get_info(JOB_NAME)['url']
#         job_last_number = server.get_info(JOB_NAME)['lastBuild']['number']
#         allure_url = job_url + str(job_last_number) + '/allure/'

#         # file_path = os.path.dirname(os.getcwd()) + "/selenium_pytest/allure-report/export/prometheusData.txt"
#         file_path = os.path.dirname(os.getcwd()) + "\selenium_pytest\allure-report\export\prometheusData.txt"
#         # content = {}
#         # with open(file_path) as f:
#         #     for i in f.readlines():
#         #         launch_name = i.strip('\n').split(' ')[0]
#         #         launch_num = i.strip('\n').split(' ')[1]
#         #         content.update({launch_name: launch_num})
#         # case_num = content['launch_retries_run']

#         # 构建飞书消息内容
#         message = {
#             "msg_type": "text",
#             "content": {
#                 "text": f"UI 执行结果：\n运行总数 {case_num}\n报告地址：\n{allure_url}"
#             }
#         }

#         # 发送消息到飞书机器人
#         response = requests.post(FEISHU_WEBHOOK, json=message)
#         if response.status_code == 200:
#             print("飞书消息发送成功")
#         else:
#             print(f"飞书消息发送失败，状态码：{response.status_code}")
#     # except FileNotFoundError:
#     #     print(f"文件不存在：{file_path}")
#     # except Exception as e:
#     #     print(f"发生错误：{e}")

# 首先确保已经安装了python-jenkins库
# pip install python-jenkins
from jenkins import Jenkins
import requests

# Jenkins服务器的地址
JENKINS_URL = "http://localhost:8080"
# Jenkins用户名
USERNAME = "monster"
# Jenkins密码
PASSWORD = "123456"
# 具体的Jenkins任务名称
JOB_NAME = "job/selenium"

# 飞书机器人Webhook地址
FEISHU_WEBHOOK = "https://open.feishu.cn/open-apis/bot/v2/hook/ed79be3d-7c65-4e2a-aa74-fc6eff47ceea"


def push_message():
    # 只发送消息，不统计其他
    try:
        # 连接到Jenkins服务器
        server = Jenkins(url=JENKINS_URL, username=USERNAME, password=PASSWORD, timeout=10)
        job_url = server.get_info(JOB_NAME)['url']
        job_last_number = server.get_info(JOB_NAME)['lastBuild']['number']
        allure_url = job_url + str(job_last_number) + '/allure/'

        # 假设这里可以通过Jenkins任务的其他方式获取到测试用例运行总数，这里暂时设置为固定值示例（你可根据实际情况修改）
        case_num = 10

        # 构建飞书消息内容
        message = {
            "msg_type": "text",
            "content": {
                "text": f"UI执行结果：\n运行总数 {case_num}\n报告地址：\n{allure_url}"
            }
        }

        # 发送消息到飞书机器人
        response = requests.post(FEISHU_WEBHOOK, json=message)
        if response.status_code == 200:
            print("飞书消息发送成功")
        else:
            print(f"飞书消息发送失败，状态码：{response.status_code}")
    except Exception as e:
        print(f"发生错误：{e}")


push_message()
# if __name__ == '__main__':
#     push_message()